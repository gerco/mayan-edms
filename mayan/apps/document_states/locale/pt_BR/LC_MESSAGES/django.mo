��    1      �  C   ,      8     9     A     N     m  #        �     �     �     �     �     �     �           
     +     J     S  (   b     �     �     �     �     �     �     �     �     �  w   �     g     n     �  
   �     �     �  +   �     �     �     �               8     T     g     v     �     �  	   �     �  �  �     s	     	     �	     �	  $   �	     �	     �	     
     
     
     $
     6
     I
  !   R
  '   t
  	   �
     �
  0   �
     �
     �
                    .     4     H     O  �   _     �     �               "     /  ;   K     �     �     �     �  !   �  !   �               .     B     Z  	   t     ~     (                           .   1                 #   /                        $                   ,   )         '                      "         %          !      0   &       
            *   -                      	   +                Comment Create state Create states for workflow: %s Create transition Create transitions for workflow: %s Create workflows Current state Date and time Datetime Delete Delete workflows Destination state Detail Detail of workflow: %(workflow)s Do transition for workflow: %s Document Document types Document types assigned the workflow: %s Edit Edit workflows Initial Initial state Is initial state? Label Last transition None Origin state Select if this will be the state with which you want the workflow to start in. Only one state can be the initial state. States States of workflow: %s Submit Transition Transitions Transitions of workflow: %s Unable to save transition; integrity error. User View workflows Workflow Workflow instance Workflow instance log entries Workflow instance log entry Workflow instances Workflow state Workflow states Workflow transition Workflow transitions Workflows Workflows for document: %s Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-27 14:11-0400
PO-Revision-Date: 2016-03-21 21:09+0000
Last-Translator: Roberto Rosario
Language-Team: Portuguese (Brazil) (http://www.transifex.com/rosarior/mayan-edms/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Comentário Criar estado Criar estados para Workflow: %s Criar Transições Criar Transição para  Workflow: %s Criar workflows Estado corrente data e hora Hora e data Excluir Excluir Workflows Estado  de destino Detalhes Detalhe do workflow: %(workflow)s Fazer a transição para o workflow: %s Documento Tipos de Documentos Os tipos de documento atribuído ao workflow: %s Editar Editar Workflows Inicial Estado Inicial é  estado inicial? Label Última transação Nenhum Estado Original Selecione se este será o estado com o qual você deseja que o fluxo de trabalho para começar em. Apenas um Estado pode ser o estado inicial. estado Estado do workflow: %s Submeter Transações Transações Transição do Workflow: %s Não foi possível salvar transição; erro de integridade. Usuário Ver workflows Workflow Instância do Workflow Workflow instance log de entradas Workflow instance log de entrada  instâncias do Workflow  Estado do Workflow Estados do Workflow Transição do Workflow Transição dos Workflows Workflows Workflows para documento: %s 