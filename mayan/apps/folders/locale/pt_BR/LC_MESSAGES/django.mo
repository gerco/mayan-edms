��          �      �           	          '     5     =     N  @   U  .   �  8   �  "   �  	   !     +     C     H     X     _     g     �  #   �  *   �     �     �  �       �     �     �     �     �     �  F     3   I  9   }  #   �  
   �     �     �                    #     @  (   F  /   o     �     �                                       	                                             
                    Add to a folder Add to folder Create folder Created Datetime created Delete Document: %(document)s added to folder: %(folder)s successfully. Document: %(document)s delete error: %(error)s Document: %(document)s is already in folder: %(folder)s. Document: %s removed successfully. Documents Documents in folder: %s Edit Edit folder: %s Folder Folders Folders containing document: %s Label Must provide at least one document. Must provide at least one folder document. Remove documents from folders Remove from folder Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-27 14:12-0400
PO-Revision-Date: 2016-03-21 21:11+0000
Last-Translator: Roberto Rosario
Language-Team: Portuguese (Brazil) (http://www.transifex.com/rosarior/mayan-edms/language/pt_BR/)
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Adicionar a uma pasta Adicione a pasta Criar pasta Criando Hora e data criada Excluir Documento: %(document)s  adicionados à pasta: %(folder)s com sucesso. Documento: %(document)s erro ao deletar: %(error)s  Documento: %(document)s  já está na pasta: %(folder)s . Documento: %s removido com sucesso. Documentos Documentos na pasta: %s Editar Editar pasta: %s Pasta Pastas Pastas contendo documento:%s Label Deve fornecer, pelo menos, um documento. Deve fornecer pelo menos um documento da pasta. Remover documentos das pastas Remover da pasta 