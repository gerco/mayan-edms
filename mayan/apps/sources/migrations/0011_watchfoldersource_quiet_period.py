# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sources', '0010_auto_20151001_0055'),
    ]

    operations = [
        migrations.AddField(
            model_name='watchfoldersource',
            name='quiet_period',
            field=models.IntegerField(default=60, help_text="Don't pick up files until they are at least this old (in seconds)", verbose_name='Quiet period'),
        ),
    ]
